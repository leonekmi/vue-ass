import { SubtitleWithStyle } from './main';

// Find events that are occuring in the same time than another event (only check previous lines)
export function intersectSubsBefore(line: SubWithIndex, lines: SubWithIndex[], alignment = false): SubWithIndex[] {
	const ret = [];
	for (const compare of lines) {
		if (compare.sub.End > line.sub.Start && compare.sub.Start < line.sub.Start) {
			if (!(alignment && compare.style.Alignment !== line.style.Alignment)) {
				ret.push(compare);
			}
		}
	}
	return ret;
}

interface SubWithIndex extends SubtitleWithStyle {
	index: number
}

interface SubWithIntersect extends SubWithIndex {
	intersect: SubWithIndex[]
}
interface SubWithPos extends SubWithIndex {
	position: number
}

interface SubtitleWithPosition extends SubtitleWithStyle {
	position: number
}

// Compute every line position based on previous lines
export function mapSubPosition(lines: SubtitleWithStyle[], alignment = false): SubtitleWithPosition[] {
	// The map is done in 2 steps to have intersectSubsBefore return array to have the index
	// We store the index in the object to retain the original index in parsed lines array
	const map = lines.map<SubWithIndex>((line, i) => {
		return {
			...line,
			index: i
		};
	}).map<SubWithIntersect>((line, _i, lines) => {
		return {
			...line,
			intersect: intersectSubsBefore(line, lines, alignment)
		};
	});
	let subsOnScreen: SubWithPos[] = [];
	const subPos: number[] = [];
	let i = 0;
	for (const line of map) {
		if (i === 0) {
			subsOnScreen.push({ ...line, position: 0 });
			subPos.push(0);
		} else {
			const still_on_screen = line.intersect.map(val => val.index);
			subsOnScreen = subsOnScreen.filter(val => still_on_screen.includes(val.index));
			let pos = 0;
			let repassNeeded = true;
			while (repassNeeded) {
				repassNeeded = false;
				pos = subsOnScreen.reduce((candidate, line) => {
					if (candidate === line.position) {
						pos++;
						repassNeeded = true;
					}
					return pos;
				}, pos);
			}
			subsOnScreen.push({ ...line, position: pos });
			subPos.push(pos);
		}
		i++;
	}
	return map.map((line, i) => {
		return {
			...line,
			intersect: undefined, // Clean useless properties
			index: undefined,
			position: subPos[i]
		};
	});
}