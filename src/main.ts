import { ParsedASSEvent } from 'ass-compiler';
import { createApp } from 'vue';
import App from './App.vue';

createApp(App).mount('#app');

export interface SubStyle {
	Name: string;
	Fontname: string;
	Fontsize: string;
	PrimaryColour: string;
	SecondaryColour: string;
	OutlineColour: string;
	BackColour: string;
	Bold: string;
	Italic: string;
	Underline: string;
	StrikeOut: string;
	ScaleX: string;
	ScaleY: string;
	Spacing: string;
	Angle: string;
	BorderStyle: string;
	Outline: string;
	Shadow: string;
	Alignment: string;
	MarginL: string;
	MarginR: string;
	MarginV: string;
	Encoding: string;
}

export interface SubtitleWithStyle {
	sub: ParsedASSEvent,
	style: SubStyle
}

export interface ComputedSubtitle extends SubtitleWithStyle {
	offset: number
}
