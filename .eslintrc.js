module.exports = {
	'env': {
		'browser': true,
		'es2021': true
	},
	'extends': [
		'eslint:recommended',
		'plugin:vue/vue3-recommended',
		'plugin:@typescript-eslint/recommended',
		'@vue/typescript'
	],
	'parserOptions': {
		'ecmaVersion': 12,
		'parser': '@typescript-eslint/parser',
		'sourceType': 'module'
	},
	parser: 'vue-eslint-parser',
	'plugins': [
		'vue',
		'@typescript-eslint'
	],
	'rules': {
		'indent': [
			'error',
			'tab'
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'always'
		],
		'vue/html-indent': [
			'error',
			'tab'
		]
	}
};
