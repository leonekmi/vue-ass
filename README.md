# Vue ASS

> "Did you Vue the ASS" is (very) probably a temporary name. If you come across here and you have a brilliant idea, get in touch!

## Philosophy, goal

There are many projects doing ASS rendering for the Web out there, the most popular one being [JavascriptSubtitlesOctopus on GitHub by Dador](https://github.com/Dador/JavascriptSubtitlesOctopus). The solution is efficient (using WASM to embed the original libass directly in the browser) and works well but it has some drawbacks that makes me want to take a different approach. First, it seems that Safari Mobile has [disabled WebAssembly for security considerations](https://www.construct.net/en/blogs/construct-official-blog-1/apple-broke-webassembly-898) in some versions of iOS. It also doesn't support strict mode (so it cannot be used in webpack environments for example). I think JSO has a way to become more appealing to people who might need ASS rendering as originally planned by the libass.

I wanted something that, while not really compliant with all the ass features, could be a working solution without WASM to avoid some problems related to that approach. [ASS.js](https://github.com/weizhenye/ASS) exists and it's a super solution. In fact, ASS.js and this project are using the [ass-compiler](https://github.com/weizhenye/ass-compiler) side-project by weizhenye to parse ASS subtitles. Unfortunately, ASS.js doesn't support karaoke tags and that's what important here [for Karaoke Mugen](https://lab.shelter.moe/karaokemugen/karaokemugen-app).

It's also my first project with Vue 3 and I wanted to try it and to make a ass renderer with the possibilites offered by a frontend framework. So it's also a way for me to skill myself with this.

## Getting started

```sh
yarn install
yarn dev
```

In `App.vue` you have the `<Subtitles>` component with the 2 props, edit it with you video and subtitle file name (place it in the root of the project folder)

Then you can click on the video to start the video and see the subtitles being rendered.

## Roadmap

This project focuses on simplicity. It includes not being able to serve all ASS features (for a start) but only those who are going to be useful in our case (@ Karaoke Mugen)

### Usable (v0.1-alpha)

- Position mechanism (alignment, no overlap, `\pos` and layers may be delayed)
- Karaoke tags (k,kf,ko)
- All color effects (primary, secondary, outline)
- `\fad` effect as a start
- Support all styles

### Support for wow effects (v0.2-alpha)

- Support `\clip`, `\move`
- Support for all fonts overrides (bold, italic, letter spacing, etc...)

### All the ASS! (v0.3-alpha)

- Support for all the ASS spec (I will try to go full css for transition, animation, i would like to avoid using JS for that)

### Cleaning up (v1.0-alpha-1)

- Fix many bugs